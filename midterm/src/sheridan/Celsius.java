package sheridan;

public class Celsius {
	
	public static int fromFahrenheit(int fahrenheit) {
		if(fahrenheit < 0) {
			throw new IllegalArgumentException();
		}
		double celsius = (fahrenheit-32)/1.8;
		return (int)celsius;
	}
}

