package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;
/*
 * @author Deep Shah
 */

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() throws IllegalArgumentException{
		assertTrue("Invalid Value",Celsius.fromFahrenheit(40) == 4);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testFromFahrenheitException() throws IllegalArgumentException {
		assertFalse("Negative value is not allowed",Celsius.fromFahrenheit(-20) == -29);
	}
	
	@Test
	public void testFromFahrenheitBoundaryIn()throws IllegalArgumentException {
		assertTrue("Invalid Value",Celsius.fromFahrenheit(54) == 12 );
	}
	
	@Test
	public void testFromFahrenheitBoundaryOut()throws IllegalArgumentException {
		assertFalse("Invalid Value",Celsius.fromFahrenheit(54) == 13);
	}

}
